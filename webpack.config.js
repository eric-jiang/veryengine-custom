const path = require("path");
const { CheckerPlugin } = require("awesome-typescript-loader");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: "./src/module/index.ts",
    devtool: "inline-source-map",
    output: {
        filename: "action_custom.js",
        path: path.resolve(__dirname, "dist"),
        library: "VR_Module",
        // libraryTarget:'commonjs',
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: "awesome-typescript-loader",
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [
        new CheckerPlugin(),
        // new webpack.optimize.UglifyJsPlugin({
        //     minimize: true,
        //     sourceMap: true,
        //     include: /\.min\.js$/,
        // }),
    ],
    resolve: {
        extensions: [".tsx", ".ts", ".js"],
    },
    optimization: {
        minimizer: [new UglifyJsPlugin()],
    },
};
