export class Trigger_Module extends VR.VE_TriggerBehaviour {

    /**
     * ID为调用触发的唯一凭证，“|”分隔表示多对一，若与触发库中其他函数重名，运行时会报警，且被舍弃；
     */
    public get ID(): string {
        return 'Compare_Test|数值比较测试';
    };

    public get className(): string {
        return 'Trigger_Module';
    };

    private _veryNumber1: VR.VeryNumber = new VR.VeryNumber();  // 定义数值变量
    private _veryNumber2: VR.VeryNumber = new VR.VeryNumber();  // 定义数值变量
    private _accuracy: VR.VeryNumber = new VR.VeryNumber();  // 定义数值变量
    private _operatorType: string = '';

    private _observer: Nullable<BABYLON.Observer<BABYLON.Scene>> = null;

    /**
     * 触发初始化时运行此函数，解析传入参数；
     * @param para_array 表格参数按逗号解析一个字符串数组传入；
     * @returns true为解析正确，false为错误；
     */
    public paraParser(para_array: string[]): boolean {
        // 传入参数个数确定，此处表示必须为4个传入参数；
        if (!VR.ParaParserUtility.ParaNumbers(this.ID, VR.ParaType.Trigger, para_array.length, 4)) {
            return false;
        }
        else {
            // 第一个参数：参数1
            let para0: VR.VeryParas = new VR.VeryParas();
            if (!VR.ParaParserUtility.NumberPara(this.projectName, this.objectID, this.ID, VR.ParaType.Trigger, para0, para_array[0])) {
                return false;
            }
            this._veryNumber1 = para0.number;

            // 第2个参数：参数2
            let para1: VR.VeryParas = new VR.VeryParas();
            if (!VR.ParaParserUtility.NumberPara(this.projectName, this.objectID, this.ID, VR.ParaType.Trigger, para1, para_array[1])) {
                return false;
            }
            this._veryNumber2 = para1.number;

            // 第3个参数：精度
            let para2: VR.VeryParas = new VR.VeryParas();
            if (!VR.ParaParserUtility.NumberPara(this.projectName, this.objectID, this.ID, VR.ParaType.Trigger, para2, para_array[2])) {
                return false;
            }
            this._accuracy = para2.number;

            // 第4个参数：比较方式
            this._operatorType = para_array[3];
            if (this._operatorType != '==' && this._operatorType != '=' && this._operatorType != '>' && this._operatorType != '<' && this._operatorType != '>=' && this._operatorType != '<=') {
                console.error(this.ID + ' 触发传入参数格式错误： + ' + para_array[3] + '，应为 “=”、“>”、“>=”、“<”或者“<=”运算符，请检查！');
                return false;
            }

            

            // 循环比较
            this._observer = this.scene.onBeforeRenderObservable.add(() => {
                switch (this._operatorType) {
                    case '==':
                        if (Math.abs(this._veryNumber1.value - this._veryNumber2.value) < this._accuracy.value) {
                            this.sendEvent();  // 满足条件，发射触发信号
                        }
                        break;
                    case '=':
                        if (Math.abs(this._veryNumber1.value - this._veryNumber2.value) < this._accuracy.value) {
                            this.sendEvent();  // 满足条件，发射触发信号
                        }
                        break;
                    case '>':
                        if (this._veryNumber1.value > this._veryNumber2.value) {
                            this.sendEvent();  // 满足条件，发射触发信号
                        }
                        break;
                    case '>=':
                        if (this._veryNumber1.value >= this._veryNumber2.value) {
                            this.sendEvent();  // 满足条件，发射触发信号
                        }
                        break;
                    case '<':
                        if (this._veryNumber1.value < this._veryNumber2.value) {
                            this.sendEvent();  // 满足条件，发射触发信号
                        }
                        break;
                    case '<=':
                        if (this._veryNumber1.value <= this._veryNumber2.value) {
                            this.sendEvent();  // 满足条件，发射触发信号
                        }
                        break;
                    default: break;
                }
            });

            return true;
        }
    }

    // 触发被销毁时，同时销毁babylon的observer对象
    public destroy(): void {
        if (this._observer) {
            this.scene.onBeforeRenderObservable.remove(this._observer);
        }
        this._observer = null;
    }
}

// 将该触发添加到函数库
VR.VE_Triggers.addTrigger(Trigger_Module);