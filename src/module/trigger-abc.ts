export class Trigger_ABC extends VR.VE_TriggerBehaviour {
    
    /**
     * ID为调用触发的唯一凭证，“|”分隔表示多对一，若与触发库中其他函数重名，运行时会报警，且被舍弃；
     */
    public get ID(): string {
        return 'ABC_Test|ABC测试';
    };

    public get className(): string {
        return 'Trigger_ABC';
    };

    public paraParser(para_array: string[]): boolean {
        return true;
    }



    public destroy(): void {
    }

}

// 将该触发添加到函数库
VR.VE_Triggers.addTrigger(Trigger_ABC);