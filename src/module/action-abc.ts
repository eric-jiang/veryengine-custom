export class Action_ABC extends VR.VE_ActionBehaviour {

    /**
     * ID为调用响应的唯一凭证，“|”分隔表示多对一，若与响应库中其他函数重名，运行时会报警，且被舍弃；
     */
    public get ID(): string {
        return 'ABC|ABC响应';
    }

    public get className(): string {
        return 'Action_ABC';
    }

    public active(): void {
        console.log('abc');
    }

    public paraParser(para_array: string[]): boolean {
        // 传入参数个数确定，此处表示必须为2个或3个传入参数；
        if (!VR.ParaParserUtility.ParaNumbers(this.ID, VR.ParaType.Action, para_array.length, 0)) {
            return false;
        } else {

            return true;
        }
    }


    public destroy(): void {

    }

}

// 将该响应添加到函数库
VR.VE_Actions.addAction(Action_ABC);