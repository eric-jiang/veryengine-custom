export class Action_Active_Module extends VR.VE_ActionBehaviour {

    /**
     * ID为调用响应的唯一凭证，“|”分隔表示多对一，若与响应库中其他函数重名，运行时会报警，且被舍弃；
     */
    public get ID(): string {
        return 'Active_Test|激活测试';
    }

    public get className(): string {
        return 'Action_Active_Module';
    }

    private _obj: VR.VeryGameObject = new VR.VeryGameObject();  // 定义物体变量
    private _isActive: VR.VeryBool = new VR.VeryBool();  // 定义bool变量
    private _childActive: VR.VeryBool = new VR.VeryBool();  // 定义bool变量
    

    /**
     * 响应激活时运行此函数，此处表示传入变量为true时，激活物体；fasle时，使物体失效；
     */
    public active(): void {
        if (this._obj.value != null) {
            if (this._childActive.value) {
                this.activeRecursion(this._obj.value.transform.transformNode, this._isActive.value);
            }
            else {
                this._obj.value.transform.transformNode?.setEnabled(this._isActive.value);
            }
        }
    }

    private activeRecursion(obj: BABYLON.Nullable<BABYLON.TransformNode>, active: boolean) {
        obj?.setEnabled(active);
        if (obj != null && obj.getChildTransformNodes().length > 0) {
            for (var i = 0; i < obj.getChildTransformNodes().length; i++) {
                this.activeRecursion(obj.getChildTransformNodes()[i], active);
            }
        }
    }

    /**
     * 响应初始化时运行此函数，解析传入参数；
     * @param para_array 表格参数按逗号解析一个字符串数组传入；
     * @returns true为解析正确，false为错误；
     */
    public paraParser(para_array: string[]): boolean {
        // 传入参数个数确定，此处表示必须为2个或3个传入参数；
        if (!VR.ParaParserUtility.ParaNumbers(this.ID, VR.ParaType.Action, para_array.length, 2, 3)) {
            return false;
        } else {
            // 参数1：是否激活
            let para1: VR.VeryParas = new VR.VeryParas(); // 因为要导出整个变量，js也没有类似out的关键字，包裹成一个对象传入以获取变量
            if (!VR.ParaParserUtility.BoolParaWithExpression(this.projectName, this.objectID, this.ID, VR.ParaType.Action, para1, para_array[0])) {
                return false;
            }
            this._isActive = para1.bool;  // 获取变量

            // 参数2：物体
            let para2: VR.VeryParas = new VR.VeryParas();
            if (!VR.ParaParserUtility.GameObjectPara(this.projectName, this.objectID, this.ID, VR.ParaType.Action, para2, para_array[1])) {
                return false;
            }
            this._obj = para2.gameObject;

            //参数3：是否设置子物体
            if (para_array.length > 2) {
                let para3: VR.VeryParas = new VR.VeryParas();
                if (!VR.ParaParserUtility.BoolParaWithExpression(this.projectName, this.objectID, this.ID, VR.ParaType.Action, para3, para_array[2])) {
                    return false;
                }
                this._childActive = para3.bool;
            } else {
                this._childActive.value = true;
            }

            return true;
        }
    }

    public destroy(): void {

    }
}

// 将该响应添加到函数库
VR.VE_Actions.addAction(Action_Active_Module);
