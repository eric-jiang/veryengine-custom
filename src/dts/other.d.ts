
// interface HandTable {
//   getData(): Array<Array<string>>;
//   width: number;
//   countEmptyRows(): number;
// }

// declare var hot1: HandTable;

// declare var projectName: string;

// declare function loadData2(): any ;

declare type Nullable<T> = T | null;


declare module Zlib {
    export class Inflate {
        constructor(p: any);
        public decompress(): any;
    }
}
declare module ossfile {
    let getJsonConfig: (projectId: string, jsonName: string) => Promise<string>;
    let getUrl: (projectId: string, assetId: string, fileName: string, hash: string) => Promise<string>;
    let getStandardUI: (projectId: string, uiName: string) => Promise<string>;
    let getJsonAsset: (projectId: string, assetId: string, fileName: string, hash?: string) => Promise<string>;
    let login: (projectID: string, refer: string) => Promise<any>;
    let upLoadJsonConfig: (jsonStr: string, projectId: string, jsonName: string) => Promise<string>;
    let getThumbnailUrl: (projectId: string, assetId: string, fileName: string, hash: string) => Promise<string>;
    let uploadFile: (file: File, projectId: string, assetId: string, fileName: string, hash?: string) => Promise<string>;
    let uploadJsonAsset: (assetStr: string, projectId: string, assetId: string, fileName: string, mode?: boolean) => Promise<string>;
    let donwLoad: (projectId: string, assetId: string, fileName: string, hash: string) => void;
    let deleteJsonAsset: (projectId: string, assetId: string, fileName: string) => Promise<string>;
    let getProjectId: () => string;
    let getLocationURL: () => string;
    let getProjectId2: () => string;
    let xuebei: any;
    let exportZip: (projectId: string, outName: string, jsonStr: string[], jsonName: string[], assetIds: string[], assetNames: string[]) => void;
    let getHiddenUrl: (resourceId: string, path: string) => string;
}

// declare var Zlib: zlib;
