
declare module VR {
    export class VeryEngine {
        init(data: string[][], project_name: string): void;
    }

    export class VeryEngineObject {
        update(): void;
    }

    export class VE_Objects {
        count: number;

        getObjectID(index: number): string;

        getVeryObject(id: string): VeryEngineObject;
    }

    export class VE_Manager {
        static dispose(): void;

        static objects(project_name: string): VE_Objects;
    }

    export class BabylonEngine {
        static Canvas: HTMLCanvasElement;
        static Engine: BABYLON.Engine;
        static Scene: BABYLON.Scene;
        static GUIScene: BABYLON.Scene;
    }

    export class Time {
        static start(): void;
    }

    export class GUIManager {
        static createCanvas(canvas: BABYLON.GUI.AdvancedDynamicTexture): void;
        static addControl(control: BABYLON.GUI.Control): void;
    }

    export class GameGlobal {
        static PlatformLoaded: boolean;

        static projectID: string;
        static projectName: string;

        static assetsData: any;

        static scenesData: any;
        static sceneIndex: number;
        static sceneName: string;

        static babylonCacheData: { [key: string]: any };
        static babylonParsedData: { [key: string]: any };

        // DO_20210311:画中画相机处理
        static minimapTextureData: { [key: string]: any };

        //DO_20210714:设置相机背景颜色
        static cameras: any[];
        static cameraDic: { [key: string]: any };

        //DO_20210928:漫游相机封装成VeryCamera
        static sceneCameraDic: { [key: string]: any };

        //DO_20211102:sprite
        static sceneSpriteDic: { [key: string]: any };
    }

    // DO_20210315:触发响应模块化扩展
    export class VE_Actions {
        public static addAction(action_class: Function): void;
    }

    export class VE_Triggers {
        public static addTrigger(trigger_class: Function): void;
    }

    export enum ParaType {
        Action = 0,
        Trigger
    }

    export class ErrorInfo {
        public isRight: boolean;
        public message: string;
        constructor();
        public clear(): void;
    }

    export interface IExpression {
        expType: string;
        className: string;
        evaluate(): any;
        clone(): IExpression;
    }

    export class VeryExpression implements IVeryVar {
        public get varType(): string;
        public get className(): string;
        public get ID(): string;
        public name: string;
        public get value(): IExpression;
        public set value(val: IExpression);
        public get isNull(): boolean;
        constructor(val: IExpression);
        public setValue(val: any): void;
        // 之后可能会有公式的情况
        public getValue(): any;
        public initValue(value_str: string, error_info: ErrorInfo): any;
        public clone(): IVeryVar;
    }

    export interface IVeryVar {
        varType: string;
        className: string;
        ID: string;
        name: string;
        setValue(val: any): void;
        getValue(): any;
        initValue(value_str: string, error_info: ErrorInfo): any;
        clone(): IVeryVar;
    }

    export class Transform {
        public get transformNode(): Nullable<BABYLON.TransformNode>;

        public get mesh(): Nullable<BABYLON.AbstractMesh>;

        public get isMesh(): boolean;

        public get isEmpty(): boolean;
        public get name(): string;
        public set name(val: string);

        private _tempVec: BABYLON.Vector3;

        public get parent(): Nullable<Transform>;

        public set parent(val: Nullable<Transform>);

        public get localPosition(): BABYLON.Vector3;

        public set localPosition(val: BABYLON.Vector3);

        public get position(): BABYLON.Vector3;

        public set position(val: BABYLON.Vector3);

        public get localEulerAngles(): BABYLON.Vector3;

        public set localEulerAngles(val: BABYLON.Vector3);

        public get localRotation(): BABYLON.Vector3;

        public set localRotation(val: BABYLON.Vector3);

        public get eulerAngles(): BABYLON.Vector3;

        public set eulerAngles(val: BABYLON.Vector3);

        public get rotation(): BABYLON.Vector3;

        public set rotation(val: BABYLON.Vector3);
        public get childCount(): number;
        public forward: BABYLON.Vector3;
        public get hierarchyCount(): number;
        public localScale: BABYLON.Vector3;
        constructor(name: string | undefined, mesh: Nullable<BABYLON.AbstractMesh>, node: Nullable<BABYLON.TransformNode>);

        public translate(translation: BABYLON.Vector3, relativeTo: BABYLON.Space): void;

        public translateRelativeTo(translation: BABYLON.Vector3, trans: Transform): void;

        public rotate(eulerAngles: BABYLON.Vector3, relativeTo: BABYLON.Space): void;

        public rotateAround(point: BABYLON.Vector3, axis: BABYLON.Vector3, speed: number): void;

        public transformPosition(local_position: BABYLON.Vector3): BABYLON.Vector3;

        public transformDirection(local_direction: BABYLON.Vector3): BABYLON.Vector3;

        public inverseTransformPosition(global_position: BABYLON.Vector3): BABYLON.Vector3;

        public inverseTransformDirection(global_direction: BABYLON.Vector3): BABYLON.Vector3;
        public destroy(): void;
    }

    export class GameObject {
        public get gameObject(): GameObject;

        public get transform(): Transform;
        public set transform(val: Transform);
        private _transform: Transform;

        public get name(): string;
        public set name(val: string);

        public get isEmpty(): boolean;

        public get mesh(): Nullable<BABYLON.AbstractMesh>;

        constructor(name: string | undefined, mesh: Nullable<BABYLON.AbstractMesh>, node: Nullable<BABYLON.TransformNode>);
        public static Find(name: string, scene?: BABYLON.Scene): Nullable<GameObject>;
        public static Destroy(obj: GameObject): void;
        public static CreateInstance(game_object: GameObject): Nullable<GameObject>;
    }

    export class VeryGameObject implements IVeryVar {
        public get varType(): string;

        public get className(): string;

        public get ID(): string;

        public name: string;

        public get value(): Nullable<GameObject>;
        public set value(val: Nullable<GameObject>);
        private _value: Nullable<GameObject>;

        public setValue(val: any): void;

        // 之后可能会有公式的情况
        public getValue(): any;

        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;
    }

    export class VeryBool implements IVeryVar {
        public get varType(): string;

        public get className(): string;

        public get ID(): string;

        public name: string;

        private _isExpression: boolean;

        public get value(): boolean;
        public set value(val: boolean);

        constructor();

        public setValue(val: any): void;

        public getValue(): any;

        public setExpression(expression: VeryExpression): void;

        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;
    }

    export class VeryNumber implements IVeryVar {
        public get varType(): string;

        public get className(): string;

        public get ID(): string;

        public name: string;

        public get value(): number;
        public set value(val: number);

        constructor();

        public setValue(val: number): void;

        public setExpression(expression: VeryExpression): void;

        public getValue(): any;
        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;
    }

    export class VeryString implements IVeryVar {
        public get varType(): string;

        public get className(): string;

        public get ID(): string;

        public name: string;

        public get value(): string;
        public set value(val: string);

        constructor();

        public getValue(): any;

        public setValue(val: any): void;

        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;

        public setExpression(expression: VeryExpression): void;
    }

    export class VeryVector3 implements IVeryVar {
        public get varType(): string;

        public get className(): string;

        public get ID(): string;

        public name: string;

        public get value(): BABYLON.Vector3;
        public set value(val: BABYLON.Vector3);

        constructor();

        public getValue(): any;

        public setValue(val: any): void;

        public setExpression(expression: VeryExpression): void;

        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;
    }

    export class VeryRect implements IVeryVar {
        public get varType(): string;

        public get className(): string;
        public get ID(): string;

        public name: string;

        public get value(): BABYLON.Vector4;
        public set value(val: BABYLON.Vector4);

        constructor();

        public getValue(): any;

        public setValue(val: any): void;

        public setExpression(expression: VeryExpression): void;

        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;
    }

    export class VeryParas {

        public gameObject: VeryGameObject;

        public bool: VeryBool;

        public number: VeryNumber;

        public string: VeryString;

        public vector3: VeryVector3;

        public vecNumbers: VeryNumber[];

        public isVec: boolean;

        public isRect: boolean;
        public rect: VeryRect;
        public rectNumbers: VeryNumber[];

        public isVec2: boolean;
        public vector2: VeryVector2;
        public vec2Numbers: VeryNumber[];

        public material: VeryMaterial;

        public list: VeryList;
        
        public uiControl: VeryUI;

        constructor();

        public clear(): void;
    }

    export class VeryVector2 implements IVeryVar {
        public get varType(): string;

        public get className(): string;
        public get ID(): string;

        public name: string;

        public get value(): BABYLON.Vector2;
        public set value(val: BABYLON.Vector2);

        constructor();

        public getValue(): any;

        public setValue(val: any): void;

        public setExpression(expression: VeryExpression): void;

        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;
    }

    export class VeryMaterial implements IVeryVar {
        public get varType(): string;

        public get className(): string;

        public get ID(): string;

        public name: string;

        public get value(): BABYLON.Nullable<BABYLON.Material>;
        public set value(val: BABYLON.Nullable<BABYLON.Material>);
        private _value: BABYLON.Nullable<BABYLON.Material>;

        constructor();

        public getValue(): any;
        public setValue(val: any): void;

        public setExpression(expression: VeryExpression): void;

        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;
    }

    export class VeryList implements IVeryVar {
        public get varType(): string;

        public get className(): string;
        public get ID(): string;

        public name: string;

        public get value(): IVeryVar[];
        public set value(val: IVeryVar[]);
        private _value: IVeryVar[];

        constructor();

        public getValue(): any;

        public setValue(val: any): void;

        public setExpression(expression: VeryExpression): void;

        public initValue(value_str: string, error_info: ErrorInfo): any;

        public clone(): IVeryVar;

        public childType: string;
        public GetValue(i: number, blue_var: IVeryVar, clone: boolean): void
        public SetValue(i: number, blue_var: IVeryVar): void;
        public Add(blue_var: IVeryVar, need_clone: boolean): void;
        public Insert(i: number, blue_var: IVeryVar, need_clone: boolean): void;
        public RemoveAt(i: number): void;
        public ValueInit(initType: Nullable<IVeryVar>, index: number[], init_value: string): void;
        public initValue(value_str: string, error_info: ErrorInfo): any;
        public SetChildValue(project_name: string, object_id: string, error_info: ErrorInfo): void;
        public Clear(): void;
    }

    export class VeryUI implements IVeryVar {
        get varType(): string;
        get className(): string;
        get ID(): string;
        name: string;
        get value(): Nullable<BABYLON.GUI.Control>;
        set value(val: Nullable<BABYLON.GUI.Control>);
        private _value;
        constructor();
        setValue(val: any): void;
        getValue(): any;
        initValue(value_str: string, error_info: ErrorInfo): any;
        GetUI(scene: BABYLON.Scene, buttonName: string): BABYLON.Nullable<BABYLON.GUI.Control>;
        clone(): IVeryVar;
    }

    export class ParaParserUtility {

        public static ParaNumbers(id: string, para_type: ParaType, current_number: number, ...required_number: number[]): boolean;

        public static GameObjectPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static GameObjectParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static StringPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static StringParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static StringParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static NumberPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static NumberParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static NumberParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static BoolPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static BoolParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static BoolParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static Vector3Para(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static Vector3OrNumberPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static Vector3ParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static Vector3OrNumberParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        private static privateNumberParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        private static privateNumberPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        private static privateNumberParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        public static RectPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static RectOrNumberPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static RectParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static RectOrNumberParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        private static privateRectNumberParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        private static privateRectNumberPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        private static privateRectNumberParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        public static Vector2Para(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static Vector2OrNumberPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static Vector2ParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static Vector2OrNumberParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        private static privateVector2NumberParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        private static privateVector2NumberPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        private static privateVector2NumberParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, index: number, log?: boolean): boolean;

        public static MaterialPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static MaterialParaWithExpression(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static MaterialParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static ListPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static ListParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;

        public static CameraPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;
        public static GetChildCamera(scene: BABYLON.Scene, name: string, log?: boolean): BABYLON.Nullable<BABYLON.Camera>;
        static CameraParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;
        public static UIControlPara(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;
        public static UIControlParaOnlyVar(project_name: string, object_id: string, id: string, para_type: ParaType, paras: VeryParas, var_str: string, log?: boolean): boolean;
        public static GetUI(scene: BABYLON.Scene, buttonName: string): BABYLON.Nullable<BABYLON.GUI.Control>;
    }

    export enum Severity {
        Warning,
        Error
    }

    export class VE_Error {

        /**
         * 实例化警告类型报错信息；
         * @param pos 错误信息位置；
         * @param msg 错误信息内容；
         * @param table_name 错误文件路径；
         */
        public static warning(pos: string, msg: string, table_name: string): VE_Error;

        /**
         * 实例化错误类型报错信息；
         * @param pos 错误信息位置；
         * @param msg 错误信息内容；
         * @param table_name 表格名；
         */
        public static error(pos: string, msg: string, table_name: string): VE_Error;

        /**
   * 实例化报错信息数据结构；
   * @param pos 错误信息位置；
   * @param message 错误信息内容；
   * @param table_name 错误文件路径；
   * @param severity 报警类型；
   */
        constructor(pos: string, message: string, table_name: string, severity: Severity);

        /**
         * 获取错误信息位置；
         */
        public getPos(): string;

        /**
         * 获取错误信息；
         */
        public getMessage(): string;

        /**
         * 获取错误信息文件路径；
         */
        public getTableName(): string;

        /**
         * 获取错误信息类型；
         */
        public getSeverity(): Severity;

        public toString(): string;
    }

    export class VE_ErrorManager {

        /// <summary>
        /// 当前错误个数；
        /// </summary>
        public static get Count(): number;

        /// <summary>
        /// 添加报错信息；
        /// </summary>
        /// <param name="error">具体报错信息数据结构；</param>
        public static Add(error: VE_Error): void;

        /// <summary>
        /// 获取具体的报错信息；
        /// </summary>
        /// <param name="index">报错信息索引；</param>
        /// <returns>具体报错信息数据结构；</returns>
        public static GetError(index: number): VE_Error;

        /// <summary>
        /// TODO: 所有报错打印，打印完删除相关信息；
        /// </summary>
        /// <param name="prefix">报错信息前缀；</param>
        public static Print(prefix?: string): void;

        /// <summary>
        /// 所有报警打印，打印完删除相关信息；
        /// </summary>
        /// <param name="prefix">报警信息前缀；</param>
        public static PrintWarnning(prefix?: string): void;

        /// <summary>
        /// 所有错误打印，打印完删除相关信息；
        /// </summary>
        /// <param name="prefix">报错信息前缀；</param>
        public static PrintError(prefix: string): void;
    }

    export enum ActionType {
        Normal, Animation
    }

    export enum SequenceActionState {
        Initial, Prepared, Running, Pause
    }

    export abstract class VE_ActionBehaviour {
        /**
         * 响应类型ID，以此ID在表格中索引当前响应，ID可多对一，中间使用“|”分隔，若与响应库中其他函数重名，运行时会报警，且被舍弃；
         */
        public abstract get ID(): string;
        /**
         * 当前响应完整类名；
         */
        public abstract get className(): string;
        /**
         * 当前响应所在场景；
         */
        public get scene(): BABYLON.Scene;
        /**
         * 当前脚本是否激活；
         */
        public get enabled(): boolean;
        /**
         * 响应所属工程名；
         */
        public get projectName(): string;
        /**
         * 响应所在GameObject；
         */
        protected gameObject: Nullable<GameObject>;
        /**
         * 响应所属对象；
         */
        protected veryObject: Nullable<VeryEngineObject>;
        /**
         * 响应所属对象名；
         */
        public get objectID(): string;
        /**
         * 当前响应名；
         */
        public get actionID(): string;
        /**
         * 响应类型，一般响应或者动画类型响应；
         */
        public get type(): ActionType;
        /**
         * 响应是否每帧运行；
         */
        public get everyFrame(): boolean;
        /**
         * 响应是否依次运行；
         */
        public isSequence: boolean;
        /**
         * 依次运行响应，当前状态；
         */
        public get SequenceState(): SequenceActionState;
        public set SequenceState(val: SequenceActionState);
        /**
         * 设置响应所属场景；
         * @param scene 当前场景；
         */
        public set(scene: BABYLON.Scene): void;

        /**
         * 响应参数解析；抽象函数，子响应必须覆盖；
         * @param para_array 响应参数数组，忽略表格中第1个响应类型ID，以第2个参数开始；
         */
        public abstract paraParser(para_array: string[]): boolean;
        /**
         * 响应启动或停止；抽象函数，子响应必须覆盖；
         */
        public abstract active(): void;
        /**
         * 响应启动/停止参数为false时，激活此函数
         */
        public onDisable(): void;
        /**
         * 帧循环函数，类似Unity的Update函数，需在子响应中覆盖使用，受everyFrame每帧运行参数控制；
         */
        public onUpdate(): void;
        /**
         * 依次运行响应控制，暂停；
         */
        public pause(): void;
        /**
         * 依次运行响应控制，恢复运行；
         */
        public resume(): void;
        /**
         * 依次运行响应控制，停止；
         */
        public stop(): void;
        /**
         * 依次运行响应控制，结束运行；
         */
        public finish(): void;
        /**
         * 如果创建过程中有异步callback获取其他创建的类，需要destroy时删除；抽象函数，子响应必须覆盖；
         */
        public abstract destroy(): void;
    }

    export abstract class VE_TriggerBehaviour {

        /**
         * 触发类型ID，以此ID在表格中索引当前触发，ID可多对一，中间使用“|”分隔，若与触发库中其他函数重名，运行时会报警，且被舍弃；
         */

        public abstract get ID(): string;

        /**
         * 当前触发完整类名；
         */
        public abstract get className(): string;

        /**
         * 当前触发所在场景；
         */
        public get scene(): BABYLON.Scene;

        /**
         * 当前脚本是否激活；
         */
        public get enabled(): boolean;
        public set enabled(val: boolean);

        /**
         * 触发所属工程名；
         */
        public get projectName(): string;

        /**
         * 当前触发脚步是否允许被触发；
         */
        public get isEnabled(): boolean;

        protected gameObject: Nullable<GameObject>;

        /**
         * 触发所属对象；
         */
        protected veryObject: Nullable<VeryEngineObject>;

        /**
         * 触发所属对象名；
         */
        public get objectID(): string;

        /**
         * 当前触发名；
         */
        public get triggerID(): string;

        /**
         * 当前：对象名.触发名；
         */
        public get unionID(): string;

        /**
         * 设置触发所属场景；
         * @param scene 当前场景；
         */
        public set(scene: BABYLON.Scene): void;

        /**
         * 触发激活，在子触发类中调用该函数，激活触发；
         */
        public sendEvent(): void;

        /**
         * 触发参数解析；抽象函数，子触发必须覆盖；
         * @param para_array 触发参数数组，忽略表格中第1个触发类型ID，以第2个参数开始；
         */
        public abstract paraParser(para_array: string[]): boolean;

        /**
         * 帧循环函数，类似Unity的Update函数，需在子触发中覆盖使用，受 触发激活条件 参数控制；
         */
        public onUpdate(): void;

        /**
         * 如果创建过程中有异步callback获取其他创建的类，需要destroy时删除；抽象函数，子触发必须覆盖；
         */
        public abstract destroy(): void;

    }

    export class Resources {

        /**
         * 根据项目资源ID加载项目资源；
         * @param asset_id 项目资源ID；
         * @returns 若成功，返回资源string；若失败，则返回错误信息；
         */
        static LoadAsset(asset_id: string): Promise<string>;

        /**
         * 根据项目资源ID加载该资源url；
         * @param asset_id 项目资源ID；
         * @returns 若成功，返回资源url；若失败，则返回错误信息；
         */
        static GetUrl(asset_id: string): Promise<string>;
    }
}
