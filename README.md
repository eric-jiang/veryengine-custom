# Web版万维引擎触发或响应模块扩展

## 请先安装Node.js开发环境

## 工程初始化

若为原始npm模块，运行以下命令，安装必要库文件
```
npm install
```

若安装了cnpm工具，关联到了国内taobao镜像（下载速度更快），可运行以下命令，安装必要库文件
```
cnpm install
```

## 编译代码，运行以下代码
```
npm run build
```
