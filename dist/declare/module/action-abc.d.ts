export declare class Action_ABC extends VR.VE_ActionBehaviour {
    /**
     * ID为调用响应的唯一凭证，“|”分隔表示多对一，若与响应库中其他函数重名，运行时会报警，且被舍弃；
     */
    get ID(): string;
    get className(): string;
    active(): void;
    paraParser(para_array: string[]): boolean;
    destroy(): void;
}
