export declare class Action_Active_Module extends VR.VE_ActionBehaviour {
    /**
     * ID为调用响应的唯一凭证，“|”分隔表示多对一，若与响应库中其他函数重名，运行时会报警，且被舍弃；
     */
    get ID(): string;
    get className(): string;
    private _obj;
    private _isActive;
    private _childActive;
    /**
     * 响应激活时运行此函数，此处表示传入变量为true时，激活物体；fasle时，使物体失效；
     */
    active(): void;
    private activeRecursion;
    /**
     * 响应初始化时运行此函数，解析传入参数；
     * @param para_array 表格参数按逗号解析一个字符串数组传入；
     * @returns true为解析正确，false为错误；
     */
    paraParser(para_array: string[]): boolean;
    destroy(): void;
}
