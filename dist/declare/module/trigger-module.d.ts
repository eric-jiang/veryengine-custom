export declare class Trigger_Module extends VR.VE_TriggerBehaviour {
    /**
     * ID为调用触发的唯一凭证，“|”分隔表示多对一，若与触发库中其他函数重名，运行时会报警，且被舍弃；
     */
    get ID(): string;
    get className(): string;
    private _veryNumber1;
    private _veryNumber2;
    private _accuracy;
    private _operatorType;
    private _observer;
    /**
     * 触发初始化时运行此函数，解析传入参数；
     * @param para_array 表格参数按逗号解析一个字符串数组传入；
     * @returns true为解析正确，false为错误；
     */
    paraParser(para_array: string[]): boolean;
    destroy(): void;
}
