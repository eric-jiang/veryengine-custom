export declare class Trigger_ABC extends VR.VE_TriggerBehaviour {
    /**
     * ID为调用触发的唯一凭证，“|”分隔表示多对一，若与触发库中其他函数重名，运行时会报警，且被舍弃；
     */
    get ID(): string;
    get className(): string;
    paraParser(para_array: string[]): boolean;
    destroy(): void;
}
